package ru.lr1.Service;

import java.io.IOException;

public interface OneServ {
    void run() throws IOException;
}
