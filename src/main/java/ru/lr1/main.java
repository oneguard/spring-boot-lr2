package ru.lr1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import org.springframework.context.annotation.ImportResource;
import ru.lr1.Service.OneServ;

import java.io.IOException;
@SpringBootApplication
@ImportResource ("classpath:bean.xml")

public class main {

    public static void main(String [] args) throws IOException {
        ApplicationContext context = SpringApplication.run(main.class, args);
        OneServ service = context.getBean(OneServ.class);
        service.run();
    }
}