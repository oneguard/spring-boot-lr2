package ru.lr1.DAOClass;

import ru.lr1.Entity.Quest;

import java.io.IOException;
import java.util.List;

public interface QuestDAO {
    List<Quest> ListQuest() throws IOException;
}
