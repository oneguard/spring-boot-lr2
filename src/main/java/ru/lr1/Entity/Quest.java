package ru.lr1.Entity;

import lombok.Data;

import java.util.List;

@Data
    public class Quest {
        private String Name;
        private List<Answer> ListAnswer;
        private String TrueAnswer;

    public Quest(String name, List<Answer> listAnswer, String trueAnswer) {
        Name = name;
        ListAnswer = listAnswer;
        TrueAnswer = trueAnswer;
    }
}



